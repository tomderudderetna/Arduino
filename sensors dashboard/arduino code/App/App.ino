#include <LiquidCrystal.h>
#include <Wire.h>
#include <Encoder.h>

/* constants and enums */
const String sep = ":";
enum { SW=5, DT=2, CLK=3}; // pin codeur
enum { RS=13, E=12, D4=11, D5=10, D6=9, D7=8}; // pin lcd

/* objects */
LiquidCrystal lcd(RS, E, D4, D5, D6, D7); // afficheur LCD
Encoder codeur(SW, DT, CLK); // codeur

/* variables */
int       potentiometer,
          photoresistor,
          joystickOffsetX,
          joystickOffsetY,
          joystickAxeX,
          joystickAxeY,
          joystickPress,
          codeurPress,
          codeurCount,
          value;
String    data;

/* SETUP */
void setup() {
  codeurCount = 0;
  data = "";
  pinMode(9, OUTPUT);
  digitalWrite(4, HIGH);
  digitalWrite(5, HIGH);
  joystickOffsetX = analogRead(A1);
  joystickOffsetY = analogRead(A2);
  Serial.begin(2000000);
  lcd.begin(16,2);
  lcd.clear();
  lcd.print("Brightness :");
  codeur.begin();
  codeur.set_hook_count_change(&perso_count_change);
}

/* LOOP */
void loop() {
  potentiometer = analogRead(A0);
  joystickAxeX = analogRead(A1)- joystickOffsetX;
  joystickAxeY = analogRead(A2)- joystickOffsetY;
  photoresistor = analogRead(A3);
  joystickPress = digitalRead(4);
  codeurPress = digitalRead(5);

  codeur.loop();

  while (Serial.available() > 0) {
    int inChar = Serial.read();
    if (inChar != '\n' && inChar != '\r')
    data += (char)inChar;
    if (inChar == '\n') {
      value = data.toInt();
      if (value != 0 || data == "0")
      {
        analogWrite(6, value * 2.55);
        lcd.setCursor(13,0);
        lcd.print("   ");
        lcd.setCursor(13,0);
        lcd.print(value);
      }
      else
      {
        lcd.setCursor(0,1);
        lcd.print("                ");
        lcd.setCursor(0,1);
        lcd.print(data);
      }
      data = "";
    }
  }
  String message = potentiometer + sep + joystickAxeX + sep + joystickAxeY + sep + joystickPress + sep + codeurCount + sep + codeurPress + sep + photoresistor;
  Serial.println(message); 
  delay(20);
}

void perso_count_change(float count) {
  if (count > 35) {
    count = 0;
    codeur.setCount(count);
  }
  if (count < 0) {
    count = 35;
    codeur.setCount(count);
  }
  codeurCount = count;
}
